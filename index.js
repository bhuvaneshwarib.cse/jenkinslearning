const express = require("express");
const path = require("path");
const ejsMate = require("ejs-mate");
const { urlencoded } = require("express");
const redditData = require("./data.json");
console.log(redditData);

const app = express();

app.engine("ejs", ejsMate);
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, "public")));
app.use(urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.redirect("/home");
});
app.get("/home", (req, res) => {
  res.render("home");
});
app.get("/r/:subreddit", (req, res) => {
  const { subreddit } = req.params;
  const data = redditData[subreddit];
  if (data) {
    res.render("subreddit", { ...data });
  } else {
    res.render("notfound", { subreddit });
  }
});
app.get("/r/:subreddits/:post", (req, res) => {
  const { subreddits, post } = req.params;
  res.send(`<h1>Post : ${post} , subreddits: ${subreddits}</h1>`);
});
app.get("/search", (req, res) => {
  const { q } = req.query;
  if (!q) {
    res.send("Not found");
  }
  res.send(`Searching the query: ${q}`);
});
app.post("/home", (req, res) => {
  const { name, email } = req.body;
  console.log(name, email);
});

app.get("/rand", (req, res) => {
  const num = Math.floor(Math.random() * 10) + 1;
  res.render("rand", { rand: num });
});
app.get("/cats", (req, res) => {
  const cats = ["cats", "dogs", "animals", "Yellow", "White"];
  res.render("cats", { cats });
});
app.get("*", (req, res) => {
  res.send("POST request");
});
app.listen(8000, () => {
  console.log("server running on port 8000");
});
